// describe("My first simple test with cypress", () => {
//     it('True should be True', () => {
//         expect(true).to.equal(true)
//     })

//     it('5 should be 5', () => {
//         expect(5).to.equal(5)
//     })
// })


// describe('Browser Actions', () => {
//     it('should load correct url', () => {
//         cy.visit('http://example.com/', { timeout: 10000 })
//     })

//     it('Should check correct url', () => {
//         cy.url().should('include', 'example.com')
//     })

//     it('should wait for 3 seconds', () => {
//         cy.wait(3000)
//     })

//     it('Should check for correct element on the page', () => {
//         cy.get('h1').should('be.visible')
//     })
// })


// describe('Browser action', () => {
//     it('Should load books website', () => {
//         cy.visit('http://books.toscrape.com/index.html', { timeout: 10000 })
//         cy.url().should('include', 'books.toscrape.com/index.html')
//         cy.log('Before Reload')
//         cy.reload()
//         cy.log('After Reload')
//     })

//     it('should click on Travel category', () => {
//         cy.get('a').contains('Travel').click()
//         cy.get('h1').contains('Travel')
//     })

//     it('should display correct number of books', () => {
//         cy.get('.product_pod').its('length').should('eq', 11)
//     })
// })


// describe('Verify Olio Price', () => {
//     it('should verify the price', () => {
//         cy.visit('http://books.toscrape.com/index.html', { timeout: 10000 })
//         cy.get('a').contains('Olio').click()
//         cy.get('.price_color').contains('£23.88')
//     })
// })

