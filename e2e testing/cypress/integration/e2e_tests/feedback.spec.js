describe('Feedback Test', () => {
    before(function () {
        cy.visit('http://zero.webappsecurity.com/index.html')
    })
    it('should display feedback content', () => {
        cy.contains('Feedback').click()
        cy.url().should('include', 'feedback.html')
        cy.get('h3').should('be.visible')
    })
    it('should fill feedback form', () => {
        cy.get('#name').type('name')
        cy.get('#email').type('email@test.com')
        cy.get('#subject').type('just subject')
        cy.get('#comment').type('comment')
    })
    it('should submit feedback form', () => {
        cy.get('.btn-signin').click()
    })
    it('should display feedback message', () => {
        cy.get('#feedback-title').contains('Feedback')
    })
})